<?php

namespace App\Http\Controllers;

use App\Entities\Article\Article;
use App\Exceptions\Http\BadRequestException;
use App\Exceptions\Http\Exception;
use App\Exceptions\Http\NotFoundException;
use App\Helpers\SortHelper;
use App\Services\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

/**
 * Контроллер для работы со статьями
 * Class ArticleController
 * @package App\Http\Controllers
 */
class ArticleController extends Controller
{
    /**
     * Возвращает список статей
     * Экшн срабатывающий на маршрут /article/list
     * @param Request $request
     * @return array
     * @throws
     */
    public function getArticlesAction(Request $request): array
    {
        $data = $this->getBodyAsArray($request);

        $limit = $data['pagination']['limit'] ?? 100;
        $page = $data['pagination']['page'] ?? 0;
        $sort = $data['sort'] ?? ["sort" => ["key" => "id", "order" => "ASC"]];

        $paginator = new PaginationService($limit, $page);

        $qb = $this->em->createQueryBuilder();
        $qb->from(Article::class, 'a');

        SortHelper::sort(Article::class, 'a', $qb, $sort);

        $qb->select('a');

        if (Arr::exists($data, 'filter')) {

            if (Arr::exists($data['filter'], 'onlyInUserLanguage')) {

                $onlyInUserLanguage = $data['filter']['onlyInUserLanguage'];

                if ($onlyInUserLanguage === true) {

                    $qb->leftJoin('a.lang', 'al');

                    /** @var \App\Services\LanguageService $languageService */
                    $languageService = app("language");

                    $userLanguage = $languageService->getLang();

                    $qb->andwhere('al.langCode = :langCode')
                        ->setParameter('langCode', $userLanguage);
                }
            }
        }
        $allArticles = $qb->getQuery()->getResult();

        /** @var int $count */
        $count = $paginator->getCountOfTotalItems($allArticles);

        $qb->setMaxResults($paginator->limit)
            ->setFirstResult($paginator->offset);

        $articles = $qb->getQuery()->getResult();

        return [
            'pagination' => [
                'currentPage' => $paginator->page,
                'pageCount' => $paginator->getTotalPages(),
                'limit' => $paginator->limit,
                'count' => $count
            ],
            'items' => $articles
        ];
    }
}
